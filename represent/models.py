from django.db import models

# Create your models here.
class Profile(models.Model):
    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)
    Address = models.CharField(max_length=100)

    def __str__(self):
        return self.firstName + " " + self.lastName
